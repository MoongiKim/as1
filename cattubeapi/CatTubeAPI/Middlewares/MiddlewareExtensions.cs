﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatTubeAPI.Middlewares
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseCateBreedMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CatBreedMiddleware>();
        }
    }
}
