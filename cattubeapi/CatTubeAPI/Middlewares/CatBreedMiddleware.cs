﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CatTubeAPI.Middlewares
{
    public class CatBreedMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly MyConfiguration _myconfig;

        public CatBreedMiddleware(RequestDelegate next, IOptions<MyConfiguration> myconfig)
        {
            _next = next;
            _myconfig = myconfig.Value;
            //Debug.WriteLine($"---> From _myconfig: {_myconfig.username}");
        }
        public async Task Invoke(HttpContext httpContext)
        {
            //Debug.WriteLine($"---> Request for {httpContext.Request.Path}");
            // Call the next middleware delegate in the pipeline.
            await _next.Invoke(httpContext);
        }
    }
}
