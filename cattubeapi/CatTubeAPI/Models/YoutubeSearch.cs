﻿/* YoutubeSearch.cs
* 
* PROG3170-19F-Sec2-Programming: Distributed Application Assignment 4
* Group project by Moon Kim(Section2) & Jaden Ji Hong Ahn(Section1)
* 
* Revision History
*          Moon Kim, 2019-11-07: Created
*/
using System;
using System.ComponentModel.DataAnnotations;

namespace CatTubeAPI.Models
{
    /// <summary>
    /// Model class to save search results of youtube
    /// </summary>
    public class YoutubeSearch
    {
        public YoutubeSearch(string channel, string kind, string title, string id)
        {
            this.Channel = channel;
            this.Kind = kind;
            this.Title = title;
            this.Id = id;
        }
        public string Channel { get; set; }
        public string Kind { get; set; }
        public string Title { get; set; }
        [Key]
        public string Id { get; set; }
    }
}
