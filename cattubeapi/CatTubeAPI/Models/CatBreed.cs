﻿/* CatBreed.cs
* 
* PROG3170-19F-Sec2-Programming: Distributed Application Assignment 4
* Group project by Moon Kim(Section2) & Jaden Ji Hong Ahn(Section1)
* 
* Revision History
*          Jaden Ji Hong Ahn, 2019-11-07: Created
*/
using System.ComponentModel.DataAnnotations;

namespace CatTubeAPI.Models
{
    /// <summary>
    /// Model class to save cat breed information from TheCatAPI.com
    /// </summary>
    public class CatBreed
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Cfa_url { get; set; }
        public string Temperament { get; set; }
        public string Origin { get; set; }
        public string Description { get; set; }
        public string Life_span { get; set; }
        public string Wikipedia_url { get; set; }
    }
}
