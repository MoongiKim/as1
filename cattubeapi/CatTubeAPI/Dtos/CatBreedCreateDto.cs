﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatTubeAPI.Dtos
{
    public class CatBreedCreateDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Cfa_url { get; set; }
        public string Temperament { get; set; }
        public string Origin { get; set; }
        public string Description { get; set; }
        public string Life_span { get; set; }
        public string Wikipedia_url { get; set; }

    }
}
