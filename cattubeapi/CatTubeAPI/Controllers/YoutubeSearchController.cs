﻿/* CatTubeController.cs
* 
* PROG3170-19F-Sec2-Programming: Distributed Application Assignment 4
* Group project by Moon Kim(Section2) & Jaden Ji Hong Ahn(Section1)
* 
* Revision History
*          Jaden Ji Hong Ahn, 2019-11-07: Created
*          Moon Kim, 2019-11.13: Modified
*           - add services
*/
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using CatTubeAPI.Models;
using Microsoft.AspNetCore.Authorization;

namespace CatTubeAPI.Controllers
{
    /// <summary>
    /// Controller to manage Youtube api
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class YoutubeSearchController : Controller
    {
        [HttpGet]
        public ActionResult<string> YouTubeSearch()
        {
            Console.WriteLine("YouTube Data API: Search");
            Console.WriteLine("========================");
            return "Please use this uri: /api/YoutubeSearch/Keyword";
        }

        // GET api/[controller]/searchkey
        [HttpGet("{searchKey}")]
        public ActionResult<List<YoutubeSearch>> YouTubeSearch(string searchKey)
        {
            Console.WriteLine("YouTube Data API: Search");
            Console.WriteLine("========================");
            List<YoutubeSearch> list = new List<YoutubeSearch>();
            try
            {
                list = Search(searchKey);
            }
            catch (AggregateException ex)
            {
                foreach (var e in ex.InnerExceptions)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }

            //Console.WriteLine("Press any key to continue...");
            //Console.ReadKey();

            return list;
        }

        private List<YoutubeSearch> Search(string searchKey)
        {
            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = "AIzaSyBKOTtHzb17azbpT6EJtNc1BqES5ksoaHA",
                ApplicationName = this.GetType().ToString()
            });

            var searchListRequest = youtubeService.Search.List("snippet");
            searchListRequest.Q = searchKey; // Replace with your search term.
            searchListRequest.MaxResults = 20;

            // Call the search.list method to retrieve results matching the specified query term.
            var searchListResponse = searchListRequest.Execute();

            //List<string> videos = new List<string>();
            //List<string> channels = new List<string>();
            //List<string> playlists = new List<string>();

            // Add each result to the appropriate list, and then display the lists of
            // matching videos, channels, and playlists.
            List<YoutubeSearch> list = new List<YoutubeSearch>();

            foreach (var searchResult in searchListResponse.Items)
            {
                YoutubeSearch item = null;
                switch (searchResult.Id.Kind)
                {
                    case "youtube#video":
                        item = new YoutubeSearch(searchResult.Id.Kind.Split("#")[0], searchResult.Id.Kind.Split("#")[1], searchResult.Snippet.Title, searchResult.Id.VideoId);
                        //videos.Add(String.Format("{0} ({1})", searchResult.Snippet.Title, searchResult.Id.VideoId));
                        break;

                    case "youtube#channel":
                        //channels.Add(String.Format("{0} ({1})", searchResult.Snippet.Title, searchResult.Id.ChannelId));
                        item = new YoutubeSearch(searchResult.Id.Kind.Split("#")[0], searchResult.Id.Kind.Split("#")[1], searchResult.Snippet.Title, searchResult.Id.VideoId);
                        break;

                    case "youtube#playlist":
                        //playlists.Add(String.Format("{0} ({1})", searchResult.Snippet.Title, searchResult.Id.PlaylistId));
                        item = new YoutubeSearch(searchResult.Id.Kind.Split("#")[0], searchResult.Id.Kind.Split("#")[1], searchResult.Snippet.Title, searchResult.Id.VideoId);
                        break;
                }
                if(item != null) list.Add(item);
            }

            return list;
            //Console.WriteLine(String.Format("Videos:\n{0}\n", string.Join("\n", videos)));
            //Console.WriteLine(String.Format("Channels:\n{0}\n", string.Join("\n", channels)));
            //Console.WriteLine(String.Format("Playlists:\n{0}\n", string.Join("\n", playlists)));
        }

        private List<CatBreed> SearchCatBreed(string searchKey)
        {
            List<CatBreed> list = new List<CatBreed>();
            //TODO: Get body from TheCatAPI.com
            return list;
        }
    }
}