﻿/* CatTubeController.cs
* 
* PROG3170-19F-Sec2-Programming: Distributed Application Assignment 4
* Group project by Moon Kim(Section2) & Jaden Ji Hong Ahn(Section1)
* 
* Revision History
*          Jaden Ji Hong Ahn, 2019-11-07: Created
*          Moon Kim, 2019-11.13
*               - add cat api services
*          Jaden Ji Hong Ahn, 2019-11-19
*               - Added authorization
*          Moon Kim, 2019-11-21
*               - modified GetCatBreed Method
*               - modified PostCatBreed Method
*               - modified PutCatBreed Method
*               - modified DeleteCatBreed Method
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CatTubeAPI.Data;
using CatTubeAPI.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using CatTubeAPI.Repositories;
using AutoMapper;
using CatTubeAPI.Dtos;
using Microsoft.AspNetCore.Authorization;

namespace CatTubeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatBreedsController : ControllerBase
    {
        private readonly CatTubeContext _context;
        private ICatBreedRepository _catBreedRepository;

        public CatBreedsController(ICatBreedRepository catBreedRepository)
        {
            //_context = context;
            _catBreedRepository = catBreedRepository;
        }

        // GET: api/CatBreeds
        [HttpGet]
        public async Task<ActionResult<List<CatBreed>>> GetCatBreedAsync()
        {
            string Baseurl = "https://api.thecatapi.com/v1/breeds";

            List<CatBreed> catBreed = new List<CatBreed>();

            using (HttpClient client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                HttpRequestMessage request = new HttpRequestMessage();
                //request.RequestUri = new Uri("");
                request.Headers.Add("x-api-key", "da764961-8649-4574-b604-19bc2e37a2ee");
                request.Method = HttpMethod.Get;

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.SendAsync(request);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var response = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    catBreed = JsonConvert.DeserializeObject<List<CatBreed>>(response);

                }

                foreach(CatBreed catbreed in catBreed)
                {
                    CatBreed catBreedFromRepo = _catBreedRepository.GetSingle(catbreed.Id);

                    if (catBreedFromRepo == null)
                    {
                        _catBreedRepository.Add(catbreed);

                        bool result = _catBreedRepository.Save();

                        if (!result)
                        {
                            //return new StatusCodeResult(500);
                            Console.WriteLine("insert error - cat Breed id: " + catbreed.Id + "-result: " + result.ToString());
                        }
                        else
                        {
                            Console.WriteLine("insert cat Breed id: " + catbreed.Id + " Name: " + catbreed.Name);
                        }
                    }
                    else
                    {
                        Mapper.Map(catbreed, catBreedFromRepo);

                        _catBreedRepository.Update(catBreedFromRepo);

                        bool result = _catBreedRepository.Save();

                        if (!result)
                        {
                            //return new StatusCodeResult(500);
                            Console.WriteLine("update error - cat Breed id: " + catbreed.Id + "-result: " + result.ToString());
                        }
                        else
                        {
                            Console.WriteLine("update cat Breed id: " + catbreed.Id + " Name: " + catbreed.Name);
                        }
                    }

                }

                //returning the employee list to view  
                //return catBreed;
                var allCatBreeds = _catBreedRepository.GetAll().ToList();

                var allCatBreedDto = allCatBreeds.Select(x => Mapper.Map<CatBreedDto>(x));

                return Ok(allCatBreedDto);

            }

            //return _context.CatBreed;
        }

        // GET: api/CatBreeds/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CatBreed>> GetCatBreed([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var catBreed = _catBreedRepository.GetSingle(id);

            if (catBreed == null)
            {
                return NotFound();
            }

            return Ok(catBreed);
        }

        // PUT: api/CatBreeds/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutCatBreed([FromRoute] string id, [FromBody] CatBreedUpdateDto catBreedUpdateDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var existingBreed = _catBreedRepository.GetSingle(id);

            if (existingBreed == null)
            {
                return NotFound();
            }

            Mapper.Map(catBreedUpdateDto, existingBreed);

            _catBreedRepository.Update(existingBreed);

            bool result = _catBreedRepository.Save();

            if (!result)
            {
                return new StatusCodeResult(500);
            }

            return Ok(Mapper.Map<CatBreedDto>(existingBreed));
        }

        // POST: api/CatBreeds
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostCatBreed([FromBody] CatBreedCreateDto catBreedCreateDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var existingBreed = _catBreedRepository.GetSingle(catBreedCreateDto.Id);

            if (existingBreed != null)
            {
                return new StatusCodeResult(500);
            }

            CatBreed toAdd = Mapper.Map<CatBreed>(catBreedCreateDto);

            _catBreedRepository.Add(toAdd);

            bool result = _catBreedRepository.Save();

            if (!result)
            {
                return new StatusCodeResult(500);
            }

            return Ok(Mapper.Map<CatBreedDto>(toAdd));
        }

        // DELETE: api/CatBreeds/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteCatBreed([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var existingSong = _catBreedRepository.GetSingle(id);

            if (existingSong == null)
            {
                return NotFound();
            }

            _catBreedRepository.Delete(id);

            bool result = _catBreedRepository.Save();

            if (!result)
            {
                return new StatusCodeResult(409);
            }

            return new StatusCodeResult(202);
        }

        private bool CatBreedExists(string id)
        {
            return _context.CatBreed.Any(e => e.Id == id);
        }
    }
}