﻿using CatTubeAPI.Data;
using CatTubeAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatTubeAPI.Repositories
{
    public class CatBreedRepository : ICatBreedRepository
    {
        private CatTubeContext _context;
        public CatBreedRepository(CatTubeContext context)
        {
            _context = context;
        }

        public IQueryable<CatBreed> GetAll()
        {
            return _context.CatBreed;
        }

        public CatBreed GetSingle(String id)
        {
            return _context.CatBreed.FirstOrDefault(x => x.Id == id);
        }

        public void Add(CatBreed item)
        {
            _context.CatBreed.Add(item);
        }

        public void Delete(string id)
        {
            CatBreed catBreed = GetSingle(id);
            _context.CatBreed.Remove(catBreed);
        }

        public void Update(CatBreed item)
        {
            _context.CatBreed.Update(item);
        }

        public bool Save()
        {
            return _context.SaveChanges() >= 0;
        }
        
    }
}
