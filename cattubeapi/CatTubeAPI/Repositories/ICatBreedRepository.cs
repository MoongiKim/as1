﻿using System;
using System.Linq;
using CatTubeAPI.Models;

namespace CatTubeAPI.Repositories
{
    public interface ICatBreedRepository
    {
        void Add(CatBreed item);
        void Delete(string id);
        IQueryable<CatBreed> GetAll();
        CatBreed GetSingle(string id);
        bool Save();
        void Update(CatBreed item);
    }
}