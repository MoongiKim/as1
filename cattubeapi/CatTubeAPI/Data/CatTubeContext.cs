﻿/* CatTubeContext.cs
* 
* PROG3170-19F-Sec2-Programming: Distributed Application Assignment 4
* Group project by Moon Kim(Section2) & Jaden Ji Hong Ahn(Section1)
* 
* Revision History
*          Jaden Ji Hong Ahn, 2019-11-07: Created
*/
using Microsoft.EntityFrameworkCore;
using CatTubeAPI.Models;

namespace CatTubeAPI.Data
{
    /// <summary>
    /// Context class to migrate and update database
    /// </summary>
    public class CatTubeContext : DbContext
    {
        public CatTubeContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<CatTubeAPI.Models.CatBreed> CatBreed { get; set; }
    }
}
